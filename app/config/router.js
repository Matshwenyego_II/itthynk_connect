import React from 'react';
import {Platform} from 'react-native';
import {StackNavigator, TabNavigator, DrawerNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

//screens
import Verification from '../screens/Verification';
import Login from '../screens/Login';
import Contacts from '../screens/Contacts';
import Profile from '../screens/Profile';

//drawerbutton
import {DrawerButton} from '../components/Header';

const LeftDrawerButton = ({navigation}) => {
    return <DrawerButton onPress={() => navigation.navigate('DrawerOpen')} />
}

export const VerificationStack = StackNavigator({
    Verification: {
        screen: Verification,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},
{headerMode:'none'});

export const LoginStack = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton   {...props}/>
        })
    },
},
{headerMode:'none'});

export const ContactsStack = StackNavigator({
    Contacts: {
        screen: Contacts,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/>
        })
    }, 
});

export const ProfileStack = StackNavigator({
    Profile: {
        screen: Profile,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/>
        })
    }, 
});

export const Tabs = TabNavigator({
    Contacts: {
        screen: ContactsStack,
         navigationOptions: {
            tabBarLabel: 'Log',
            tabBarIcon: ({tintColor}) => <Icon name="ios-book-outline" size={20} color={tintColor} />
        }
    },
    Profile: {
        screen: ProfileStack,
        navigationOptions: {
            tabBarLabel: 'Welcome',
            tabBarIcon: ({tintColor}) => <Icon name="ios-person" size={20} color={tintColor} />
        }
    },
},
{ 
    tabBarPosition: 'bottom',
    animationEnabled: true,
    tabBarOptions: {
        activeTintColor: 'white',
        activeBackgroundColor: 'white',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: '#08A3C7',
        },
        showIcon: true,
        showLabel: false
    }

});

export const Drawer = DrawerNavigator({
    Verification: {
        screen: VerificationStack,
        navigationOption: {
            drawerLabel: 'Verification'
        }
    },
    Contacts: {
        screen: Tabs,
        navigationOption: {
            drawerLabel: 'Contacts'
        }
    },
    Login: {
        screen: LoginStack,
        navigationOption: {
            drawerLabel: 'Login'
        }
    },
},{
    drawerBackgroundColor:'#08A3C7',
    activeTintColor:'#2196f3',
    activeBackgroundColor:'rgba(0, 0, 0, .04)', 
    inactiveTintColor:'rgba(0, 0, 0, .87)',
    inactiveBackgroundColor:'transparent',
    backgroundColor: '#000000',
    color: '#ffffff'
});