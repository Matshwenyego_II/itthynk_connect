import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    ToastAndroid
} from 'react-native';
import { Button } from 'react-native-elements';


import background from '../components/images/back2.jpg';
import logo from '../components/images/Logo.png';
const base = "https://itthynk-connect.herokuapp.com";


export default class Login extends Component{
 

    constructor(){
        super();
        
        this.state = {
            email: null,
            password: null,
        }
    }

    onSubmit(){
        console.log('Logging in')
    
        let obj ={
            "email": this.state.email,
            "password": this.state.password,
        }
    
        console.log(obj);
        fetch(base + '/api/user/login',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
    
                body: JSON.stringify(obj)
    
            }).then((response)=> {
                console.log('response', response)
                  if(response.status == 200){ 
                        ToastAndroid.show('Successfully logged in!!', ToastAndroid.TOP, ToastAndroid.LONG);
                        this.props.navigation.navigate('Contacts');  
                        return response.json(); 
                  }
                  else{ 
                    ToastAndroid.show('User not verified!!//User and/or password is incorrect!!', ToastAndroid.TOP, ToastAndroid.LONG);
                    this.props.navigation.navigate('Login');  
                    return response.json(); 
                }
                             
            }).then(result=>{
                return result.json();
                 
            })
    }
    
    render(){
     
        return(
            
                <Image source={background} style={styles.container}>
                <Image source={logo} style={styles.logo}/>

                <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    keyboardType={'email-address'}
                    placeholder={'Email address'}                    
                    onChangeText={(text) =>{this.setState({email: text})}}
                />

                <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    keyboardType={'numeric'}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    onChangeText={(text) =>{this.setState({password: text})}}
                />

                <View style={styles.spacer}>
                <Button
                    raised
                    style ={styles.verifyButton}    
                    icon={{name: 'fingerprint', type: 'material-community'}}
                    title='LOGIN'
                    backgroundColor={'#08A3C7'}
                    borderRadius={9}
                    onPress={ () => this.onSubmit() }

                />
                </View>
                </Image>
        
        )
    }
}

const styles = StyleSheet.create({
    spacer:{
        marginTop:80
    },
    verifyButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0
    },
    container:{
       
        flex:1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
        
    },
    logo:{
        marginTop:100,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        
    },
    verifyInput:{
        margin: 15,
        borderStyle:'solid',
        borderWidth: 2,
        borderRadius: 9,
        borderColor:'#fff',
        textAlign: 'center',
    },
    text:{
        marginTop: -5,
        alignSelf: 'center', 
        color: 'black', 
        fontSize: 13, 
        fontWeight: 'bold',
        
    },


})