import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    ToastAndroid
} from 'react-native';
import { Button } from 'react-native-elements';


import background from '../components/images/back2.jpg';
import logo from '../components/images/Logo.png';
const base = "https://itthynk-connect.herokuapp.com";



export default class Verfication extends Component{
 

    constructor(){
        super();
        
        this.state = {
            code: null,
        }
    }

    onVerification(){
        console.log('Submitting code')
    
        let obj ={
            "code": this.state.code,
        }
    
        console.log(obj);
        fetch(base + '/api/user/verify',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
    
                body: JSON.stringify(obj)
    
            }).then((response)=> {
                console.log('response', response)
                  if(response.status == 200){ 
                        ToastAndroid.show('Account varified!!', ToastAndroid.TOP, ToastAndroid.LONG);  
                        this.props.navigation.navigate('Contacts');
                        return response.json(); 
                  }           
            }).then(result=>{
                return result.json();
                 
            })
    }
    
    render(){
     
        return(
            
                <Image source={background} style={styles.container}>
                <Image source={logo} style={styles.logo}/>

                <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    placeholder={'Enter Verification Code'}
                    onChangeText={(text) =>{this.setState({code: text})}}
                />
                <TouchableOpacity                             
                    onPress={ () => this.props.navigation.navigate('Login')}
                >
                <Text style={styles.text}>Are you a verified user?</Text>
                </TouchableOpacity>               
                <View style={styles.spacer}>
                <Button
                    raised
                    style ={styles.verifyButton}    
                    icon={{name: 'barcode-scan', type: 'material-community'}}
                    title='VERIFY ACCOUNT'
                    backgroundColor={'#08A3C7'}
                    borderRadius={9}
                    onPress={ () => this.onVerification() }

                />
                </View>
                </Image>
        
        )
    }
}

const styles = StyleSheet.create({
    spacer:{
        marginTop:100
    },
    verifyButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0
    },
    container:{
       
        flex:1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
        
    },
    logo:{
        marginTop:100,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        
    },
    verifyInput:{
        margin: 20,
        borderStyle:'solid',
        borderWidth: 2,
        borderRadius: 9,
        borderColor:'#fff',
        textAlign: 'center',
    },
    text:{
        marginTop: -5,
        alignSelf: 'center', 
        color: 'black', 
        fontSize: 15, 
        fontWeight: 'bold',
        
    },


})