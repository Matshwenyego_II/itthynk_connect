import React, {Component} from 'react';
import {
    View,
    Text,
    ListView,
    StyleSheet,
    Image,
    ScrollView    
} from 'react-native';


import avatar from '../components/images/Avatar.jpg';


export default class Contacts extends Component{
    render(){
        return(
            <ScrollView>
            <View style={styles.view}>
            <View style={styles.info}>
                <Text>Lucky Malale Makhanya</Text>
                <Text>Software Developer</Text>
                <Text>(+27)83 892 4389</Text>
            </View>
            <Image style={styles.image} source={avatar}/>    
            </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    view :{
        backgroundColor: 'white',
        margin:10,       
        borderWidth: 2,
        borderColor: '#08A3C7',
        height: 80,
        borderRadius: 9
    },
    image:{
        margin: 10,
        marginTop: -55,
        height:60,
        width: 60,
        borderRadius: 9
    },
    info:{
        marginLeft: 100,
        marginTop: 5,
        
        
    }

    
})



  
